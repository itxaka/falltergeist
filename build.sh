#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "${STEAM_APP_ID_LIST}"

readonly pfx="${PWD}/local"
mkdir -p "${pfx}"

# build falltergeist
pushd "source"
cmake -DGLM_INCLUDE_DIR=../glm -DOpenGL_GL_PREFERENCE=GLVND -DCMAKE_INSTALL_PREFIX="${pfx}" .
make -j "$(nproc)"
make install
popd

# place resulting binaries in directories: '<app_id>/dist'
for app_id in ${STEAM_APP_ID_LIST} ; do
  mkdir -p "${app_id}/dist/share/falltergeist/data/"
  cp -v "${pfx}"/bin/falltergeist "${app_id}"/dist/
  cp -rfv "${pfx}"/share/falltergeist/data/ "${app_id}"/dist/
done
